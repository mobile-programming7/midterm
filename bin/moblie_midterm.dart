import 'dart:ffi';
import 'package:moblie_midterm/moblie_midterm.dart' as moblie_midterm;
import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  String pos = "8-2+(3*4)/(2^2)";
  print(inFix(pos));

  print(inFixToPostfix(pos));

  print(evaluatePostfix(inFixToPostfix(pos)));

}

List inFix(String s){

  var ch = [];
  String char;
  for(int i=0; i<s.length; i++){
    char = s.substring(i,i+1);
    ch.add(char);
  }

  return ch;
}


int oper(String c) {
  switch (c) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
      return 2;
    case "^":
      return 3;
  }
  return -1;
}

String inFixToPostfix(String s) {
  String result = ("");

  var ch = [];
  var aa = inFix(s);

  for (int i = 0; i < s.length; ++i) {
    String c = aa[i];

    if (c.contains(new RegExp(r'[a-zA-Z0-9]'))) {
      result += c;
    } else if (c == "(") {
      ch.add("(");
    } else if (c == ")") {
      while (!ch.isEmpty && ch.last != "(") {
        result += ch.last;
        ch.removeLast();
      }
      ch.removeLast();
    } else {
      while (!ch.isEmpty && oper(c) <= oper(ch.last)) {
        result += ch.last;
        ch.removeLast();
      }
      ch.add(c);
    }
  }

  while (!ch.isEmpty) {
    if (ch.last == '(') {
      return "Invalid Expression";
    }
    result += ch.last;
    ch.removeLast();
  }

  return result;
}

double evaluatePostfix(String s){

  var ch = [];
  var aa = inFix(s);

  for(int i=0; i<s.length; i++){
    String c = aa[i];
    if (c.contains(new RegExp(r'[a-zA-Z0-9]'))){  
      ch.add(double.parse(c));
    }else{
      double left = ch.removeLast();
      double right = ch.removeLast();

      switch(c){
        case "+":
        ch.add(right+left);
        break;
        
        case "-":
        ch.add(right-left);
        break;

        case "/":
        ch.add(right/left);
        break;

        case "*":
        ch.add(right*left);
        break;

        case "^":
        ch.add(pow(right, left));
        break;

      }
    }
  }
  return ch.removeLast();
}